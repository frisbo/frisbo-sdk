<?php

namespace Frisbo\FrisboSdk;

use Exception;
use Frisbo\FrisboSdk\Exceptions\AccessForbiden;
use Frisbo\FrisboSdk\FrisboCache;
use Frisbo\FrisboSdk\Models\Filters\Filter;
use Frisbo\FrisboSdk\Models\Organization;
use Frisbo\FrisboSdk\Models\Channel;
use Frisbo\FrisboSdk\Models\Order;
use Frisbo\FrisboSdk\Models\Product;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * FrisboClient helper
 */
abstract class FrisboV1Client implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected $apiUrl = 'https://api.frisbo.ro';

    const AUTH_URL = '/v1/auth/login';
    const ORGANIZATIONS_URL = '/v1/organizations';
    const ORGANIZATIONS_CHANNEL_URL = '/v1/organizations/{organizationId}/channels';
    const ORGANIZATIONS_CHANNEL_STOCK_URL = '/v1/organizations/{organizationId}/channels/{channelId}/storage/stock';
    const ORGANIZATIONS_PRODUCT_URL = '/v1/organizations/{organizationId}/products';
    const ORGANIZATIONS_ORDERS_URL = '/v1/organizations/{organizationId}/orders';
    const ORGANIZATIONS_ORDER_URL = '/v1/organizations/{organizationId}/orders/{orderId}';
    const TRANSITION_ORDER_URL = '/v1/organizations/{organizationId}/orders/{orderId}/workflows/order_workflow/transitions/{transitionName}';

    private $_frisboCache;
    private $options;

    public function __construct(array $options, FrisboCache $frisboCache)
    {
        $this->_frisboCache = $frisboCache;
        $this->options = $options;
        $this->apiUrl = getenv('FRISBO_API_URL') ? getenv('FRISBO_API_URL') : $this->apiUrl;
        $this->setLogger(new \Psr\Log\NullLogger());
    }

    protected function getAccessToken(bool $force = false):? string
    {
        if (!$force) {
            $cachedToken = $this->_frisboCache->load(FrisboCache::FRISBO_TOKEN_KEY);
            if (!empty($cachedToken)) {
                return $cachedToken;
            }
        }

        return $this->refreshToken();
    }

    protected function refreshToken()
    {
        if(!array_key_exists('username', $this->options) || !array_key_exists('password', $this->options)) {
            throw new AccessForbiden("No username or password set");
        }

        $username = $this->options['username'];
        $password = $this->options['password'];
        try {
            $authJsonResponse = $this->login($username, $password);
            $authResponse = json_decode($authJsonResponse);
            $this->_frisboCache->save(FrisboCache::FRISBO_TOKEN_KEY, $authResponse->access_token, $authResponse->expires_in);
            return $authResponse->access_token;
        } catch(Exception $ex) {
            $this->logger->info('FrisboClient::'.$ex->getMessage());
        }

        $this->forgetToken();
        return null;
    }

    protected function forgetToken()
    {
        $this->_frisboCache->forget(FrisboCache::FRISBO_TOKEN_KEY);
    }

    /**
     * Login via username and password and return json
     *
     * @param string $username
     * @param string $password
     * @return string|null
     */
    abstract public function login(string $username = null, string $password = null):? string;

    /**
     * Gets the current user's organizations
     *
     * @return Organization[]
     */
    abstract public function getOrganizations(): array;

    /**
     * Gets the selected organization's channels
     *
     * @param integer $organizationId
     * @return Channel[]
     */
    abstract public function getOrganizationChannels(int $organizationId): array;

    /**
     * Sends the product to Frisbo
     *
     * @param int $organizationId
     * @param Product $product
     * @return Product
     */
    abstract public function sendProduct($organizationId, Product $product): Product;

    /**
     * Sends the order to Frisbo
     *
     * @param int $organizationId
     * @param Order $order
     * @return Order
     */
    abstract public function sendOrder($organizationId, Order $order): Order;

    /**
     * Updates the order in Frisbo
     *
     * @param int $organizationId
     * @param Order $order
     * @return Order
     */
    abstract public function updateOrder($organizationId, Order $order): Order;

    /**
     * Cancels the order
     *
     * @param int $organizationId
     * @param Order $order
     * @return Order
     */
    abstract public function cancelOrder($organizationId, Order $order): Order;

    /**
     * Transitions order
     *
     * @param int $organizationId
     * @param Order $order
     * @return Order
     */
    abstract public function transitionOrder($organizationId, Order $order, string $transitionName): ?string;

    /**
     * Get products by Filter
     *
     * @param int $organizationId
     * @param Filter ...$filter
     * @return Product[]
     */
    abstract public function getProductsByFilter($organizationId, Filter ...$filters): array;

    /**
     * Get orders by Filter
     *
     * @param int $organizationId
     * @param Filter ...$filter
     * @return Order[]
     */
    abstract public function getOrdersByFilter($organizationId, Filter ...$filters): array;

    /**
     * Get stock form stock umbrella
     *
     * @param int $organizationId
     * @param int $channelId
     * @param Filter ...$filter
     * @return Storage[]
     */
    abstract public function getStocksByFilter($organizationId, $channelId, Filter ...$filters): array;
}
