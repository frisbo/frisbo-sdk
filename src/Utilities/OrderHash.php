<?php

namespace Frisbo\FrisboSdk\Utilities;

use Frisbo\FrisboSdk\Models\Order;
use Frisbo\FrisboSdk\Models\OrderProduct;
use Frisbo\FrisboSdk\Models\OrderAddress;
use Frisbo\FrisboSdk\Models\OrderCustomer;

class OrderHash
{

    /**
     * Gets a hash for an order. Useful to know if to issue an update or not
     *
     * @param Order $order
     * @return string
     */
    public function getOrderHash(Order $order): string
    {
        $valueToHash = "";
        $valueToHash .= $order->order_reference;
        $valueToHash .= ($order->discount ? number_format(round($order->discount, 2), 2, '.', ''): 'd');
        $valueToHash .= ($order->transport_tax ? number_format(round($order->transport_tax, 2), 2, '.', '') : 't');
        $valueToHash .= $this->getAddressHash($order->billing_address);
        $valueToHash .= $this->getAddressHash($order->shipping_address);
        $valueToHash .= $this->getCustomerHash($order->shipping_customer);
        $valueToHash .= $this->getCustomerHash($order->billing_customer);
        $valueToHash .= $this->getProductsHash($order->products);
        // WARNING: do not change this order of hashing if you don't know what you're doing
        return md5($valueToHash);
    }

    /**
     * Get OrderProducts hash
     *
     * @param OrderProduct[] $orderProducts
     * @return string
     */
    private function getProductsHash(array $orderProducts): string
    {
        $valueToHash = "";

        $hashes = array_map(
            function ($orderProduct) {
                return ($orderProduct->name ?? "n") 
                    . number_format(round($orderProduct->quantity, 2), 2, '.', '') 
                    . number_format(round($orderProduct->price_with_vat, 2), 2, '.', '') 
                    . number_format(round($orderProduct->discount ?? 0, 2), 2, '.', '') 
                    . number_format(round($orderProduct->vat, 2), 2, '.', '') 
                    . ($orderProduct->is_virtual ? '1': '0');
            },
            $orderProducts
        );
        sort($hashes);
        return md5($valueToHash.implode('', $hashes));
    }

    /**
     * Gets hash from address
     *
     * @param OrderAddress $orderAddress
     * @return string
     */
    private function getAddressHash($orderAddress): string
    {
        $valueToHash = $orderAddress->street . $orderAddress->city . $orderAddress->county . $orderAddress->country . $orderAddress->zip;
        return md5($valueToHash);
    }

    /**
     * Gets hash from customer
     *
     * @param OrderCustomer $orderAddress
     * @return string
     */
    private function getCustomerHash($orderCustomer): string
    {
        $valueToHash = $orderCustomer->first_name . $orderCustomer->last_name . $orderCustomer->phone . $orderCustomer->trade_register_registration_number . $orderCustomer->vat_registration_number . $orderCustomer->email;
        return md5($valueToHash);
    }

}