<?php

namespace Frisbo\FrisboSdk;

abstract class FrisboCache
{
    const CACHE_PREFIX = 'FRISBO_CACHE';
    const CACHE_LIFETIME = 86400;

    /**
     * Cache key used for Frisbo auth token
     */
    const FRISBO_TOKEN_KEY = 'frisbo_cache_token';

    protected $prefix;
    protected $options;

    public function __construct(string $prefix = self::CACHE_PREFIX, array $options = []) {
        $this->prefix = $prefix;
        $this->options = $options;
    }

    /**
     * @param $cacheId
     * @return bool|string
     */
    abstract public function load(string $key);

    /**
     * @param string $key
     * @param string $value
     * @param int $cacheLifetime
     * @return bool
     */
    abstract public function save(string $key, string $value, int $cacheLifetime = self::CACHE_LIFETIME): bool;

    /**
     * Forget cache key
     *
     * @param string $key
     * @return boolean
     */
    abstract public function forget(string $key): bool;
}
