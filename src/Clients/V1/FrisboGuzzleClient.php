<?php

namespace Frisbo\FrisboSdk\Clients\V1;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\BadResponseException;

use Exception;
use Frisbo\FrisboSdk\Exceptions\AccessForbiden;
use Frisbo\FrisboSdk\Exceptions\ResponseErrorHandler;
use Frisbo\FrisboSdk\FrisboV1Client;
use Frisbo\FrisboSdk\Models\Channel;
use Frisbo\FrisboSdk\Models\Filters\Filter;
use Frisbo\FrisboSdk\Models\Order;
use Frisbo\FrisboSdk\Models\Organization;
use Frisbo\FrisboSdk\Models\Product;
use Frisbo\FrisboSdk\Models\Storage;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class FrisboGuzzleClient extends FrisboV1Client implements LoggerAwareInterface
{
    use LoggerAwareTrait, ResponseErrorHandler;

    /**
     * Login via username and password and return json
     *
     * @param string $username
     * @param string $password
     * @return string|null
     */
    public function login(string $username = null, string $password = null):? string
    {
        if ($username == null || $password == null) {
            return null;
        }

        $credentialsBody = [
            'email' => $username,
            'password' => $password
        ];

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'POST', 
                $this->apiUrl.self::AUTH_URL,
                [
                    RequestOptions::JSON => $credentialsBody
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->handleClientException($e, 'login');   
        } catch (\Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function getOrganizations(): array
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                $this->apiUrl . self::ORGANIZATIONS_URL,
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer '.$accessToken]
                ]
            );

            $organizationsArray = json_decode($response->getBody());
            return array_map(function($organization) {
                return Organization::fromObject($organization);
            }, $organizationsArray);
        } catch (BadResponseException $e) {
            $this->handleClientException($e, 'organizations');
        } catch (\Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function getOrganizationChannels(int $organizationId): array
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                $this->apiUrl . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_CHANNEL_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            $channelsArray = json_decode($response->getBody());
            return array_map(function($organization) {
                return Channel::fromObject($organization);
            }, $channelsArray);
        } catch (BadResponseException $e) {
            $this->handleClientException($e, 'channel');
        } catch (\Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function sendProduct($organizationId, Product $product): Product
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'POST',
                $this->apiUrl . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_PRODUCT_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                    RequestOptions::JSON => (array) $product
                ]
            );

            $productResponse = json_decode($response->getBody());
            return Product::fromObject($productResponse);
        } catch (BadResponseException $e) {
            $this->logger->info('FrisboClient::'.$e->getMessage());
            return $this->handleClientException($e, 'product');
        } catch (Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function sendOrder($organizationId, Order $order): Order
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'POST',
                $this->apiUrl . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_ORDERS_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                    RequestOptions::JSON => (array) $order
                ]
            );

            $orderResponse = json_decode($response->getBody());
            return Order::fromObject($orderResponse);
        } catch (BadResponseException $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            $this->handleClientException($e, 'order');
        } catch (Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function updateOrder($organizationId, Order $order): Order
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'PUT',
                $this->apiUrl . str_replace(['{organizationId}','{orderId}'], [$organizationId, $order->order_id], self::ORGANIZATIONS_ORDER_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                    RequestOptions::JSON => (array) $order
                ]
            );

            $orderResponse = json_decode($response->getBody());
            return Order::fromObject($orderResponse);
        } catch (BadResponseException $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            $this->handleClientException($e, 'order');
        } catch (Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function cancelOrder($organizationId, Order $order): Order
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'DELETE',
                $this->apiUrl . str_replace(['{organizationId}', '{orderId}'], [$organizationId, $order->order_id], self::ORGANIZATIONS_ORDER_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                    RequestOptions::JSON => (array) $order
                ]
            );

            $orderResponse = json_decode($response->getBody());
            return Order::fromObject($orderResponse);
        } catch (BadResponseException $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            $this->handleClientException($e, 'order');
        } catch (Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function transitionOrder($organizationId, Order $order, string $transitionName): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $this->logger->info($this->apiUrl . str_replace(['{organizationId}', '{orderId}', '{transitionName}'], [$organizationId, $order->order_id, $transitionName], self::TRANSITION_ORDER_URL));

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'POST',
                $this->apiUrl . str_replace(['{organizationId}', '{orderId}', '{transitionName}'], [$organizationId, $order->order_id, $transitionName], self::TRANSITION_ORDER_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            $this->handleClientException($e, 'order');
        } catch (Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function getProductsByFilter($organizationId, Filter ...$filters): array
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                $this->apiUrl . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_PRODUCT_URL) . '?'. Filter::combineFilters(...$filters),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            $productResponse = json_decode($response->getBody());
            return array_map(
                function ($product) {
                    return Product::fromObject($product);
                }, 
                $productResponse->data
            );
        } catch (BadResponseException $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            $this->handleClientException($e, 'product');
        } catch (Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function getOrdersByFilter($organizationId, Filter ...$filters): array
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                $this->apiUrl . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_ORDERS_URL) . '?' . Filter::combineFilters(...$filters),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            $orderResponse = json_decode($response->getBody());
            return array_map(
                function ($order) {
                    return Order::fromObject($order);
                }, 
                $orderResponse->data
            );
        } catch (BadResponseException $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            return $this->handleClientException($e, 'orders');
        } catch (Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }

    public function getStocksByFilter($organizationId, $channelId, Filter ...$filters): array
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                $this->apiUrl . str_replace(['{organizationId}', '{channelId}'], [ $organizationId, $channelId], self::ORGANIZATIONS_CHANNEL_STOCK_URL) . '?' . Filter::combineFilters(...$filters),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            $storageResponse = json_decode($response->getBody());
            return array_map(
                function ($storage) {
                    return Storage::fromObject($storage);
                },
                $storageResponse
            );
        } catch (BadResponseException $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            $this->handleClientException($e, 'storage');
        } catch (Exception $e) {
            $this->logger->info('FrisboClient::' . $e->getMessage());
            throw $e;
        }
    }
}