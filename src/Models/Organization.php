<?php

namespace Frisbo\FrisboSdk\Models;

class Organization extends JsonConvertible
{
    public $organization_id;
    public $name;
}