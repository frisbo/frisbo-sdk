<?php

namespace Frisbo\FrisboSdk\Models;

class Storage extends JsonConvertible
{
    public $product_id;
    public $scriptic_stock;
    public $reserved_stock;
    public $available_stock;
    public $updated_at;

    /**
     *
     * @var Product
     */
    public $product;
}
