<?php

namespace Frisbo\FrisboSdk\Models;

class Product extends JsonConvertible
{
    public $product_id;
    public $organization_id;
    public $name;
    public $type;
    public $sku;
    public $ean;
    public $upc;
    public $has_serial_number;
    public $external_code;
    public $vat;

    /**
     * @var Storage[]
     */
    public $storage;

    /**
     * @var ProductDimensions
     */
    public $dimensions;

    /**
     * If product is package, this shows the components
     *
     * @var Product[]
     */
    public $products;
}
