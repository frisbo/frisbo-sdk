<?php

namespace Frisbo\FrisboSdk\Models;

class ProductDimensions extends JsonConvertible
{
    public $height;
    public $width;
    public $length;
    public $weight;
}
