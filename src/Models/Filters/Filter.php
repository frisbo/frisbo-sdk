<?php

namespace Frisbo\FrisboSdk\Models\Filters;

class Filter
{

    public $key;
    public $value;
    public $comparisonOperator;

    public function __construct(string $key, string $value, string $comparisonOperator='')
    {
        $this->key = $key;
        $this->value = $value;
        $this->comparisonOperator = $comparisonOperator;
    }

    public function make()
    {
        if (empty($this->comparisonOperator)) {
            return $this->key . '=' . $this->value;
        }
        return $this->key.'_'.$this->comparisonOperator.'='.$this->value;
    }

    public static function from(string $key, string $value, string $comparisonOperator = ''): Filter
    {
        return new self($key, $value, $comparisonOperator);
    }
    
    public static function combineFilters(self ...$filters): string
    {
        return array_reduce(
            $filters, 
            function ($carry, $currentFilter) {
                return $carry . $currentFilter->make() . '&';
            }, 
            ''
        );
    }

}