<?php

namespace Frisbo\FrisboSdk\Models;

class Channel extends JsonConvertible
{
    public $id;
    public $name;
}
