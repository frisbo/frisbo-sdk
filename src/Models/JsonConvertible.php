<?php

namespace Frisbo\FrisboSdk\Models;

use stdClass;

abstract class JsonConvertible
{
    static function fromJson($json)
    {
        $objJson = json_decode($json);
        return self::fromObject($objJson);
    }

    static function fromObject(stdClass $objJson)
    {
        $result = new static();
        $class = new \ReflectionClass($result);
        $publicProps = $class->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach ($publicProps as $prop) {
            $propName = $prop->name;
            if (isset($objJson->$propName)) {
                $prop->setValue($result, $objJson->$propName);
            } else {
                $prop->setValue($result, null);
            }
        }
        return $result; 
    }
    
    /**
     * Converts object to json
     *
     * @return void
     */
    public function toJson()
    {
        return json_encode($this);
    }
}