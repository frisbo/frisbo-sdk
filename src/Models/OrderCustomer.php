<?php

namespace Frisbo\FrisboSdk\Models;

class OrderCustomer extends JsonConvertible
{
    public $first_name;
    public $last_name;
    public $email;
    public $phone;
    public $vat_registration_number;
    public $trade_register_registration_number;

    public static function create(
        string $first_name, 
        string $last_name, 
        string $phone, 
        string $email, 
        string $vat_registration_number = null, 
        string $trade_register_registration_number = null
    ) {
        return self::fromObject(
            (object) [
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone' => $phone,
                'email' => $email,
                'vat_registration_number' => $vat_registration_number,
                'trade_register_registration_number' => $trade_register_registration_number
            ]
        );
    }
}
