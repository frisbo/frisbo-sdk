<?php

namespace Frisbo\FrisboSdk\Exceptions;

use GuzzleHttp\Exception\BadResponseException;

trait ResponseErrorHandler
{
    private function handleProductErrorResponse(object $productResponse)
    {
        $hasError = (bool) ($productResponse->error ?? false);
        if ($hasError) {
            $productMessage = $productResponse->message;
            if (strpos($productMessage, 'already exists') !== false) {
                throw new ProductExists("Product already exists in Frisbo.");
            }
        }

        throw new ServerErrorException("Unknown error");
    }

    private function handleOrderErrorResponse(object $orderResponse)
    {
        $hasError = (bool) ($orderResponse->error ?? false);
        if ($hasError) {
            $this->throwOrderExistsException($orderResponse->message);
            throw new OrderException($orderResponse->message);
        }
        
        throw new ServerErrorException("Unknown error");
    }

    private function throwOrderExistsException(string $message)
    {
        if (strpos($message, 'already exists') !== false) {
            throw new OrderExists($message);
        }
    }

    private function handleClientException(BadResponseException $e, string $entityType):? string
    {
        if ($e->hasResponse()) {
            if ($e->getResponse()->getStatusCode() == '403') {
                throw new AccessForbiden("Invalid username or password");
            }
            $response = json_decode($e->getResponse()->getBody());
            switch($entityType) {
                case 'order':
                    $this->handleOrderErrorResponse($response);
                case 'product':
                    $this->handleProductErrorResponse($response);
            }
        }

        throw $e;
    }
}