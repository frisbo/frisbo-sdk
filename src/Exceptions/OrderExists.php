<?php

namespace Frisbo\FrisboSdk\Exceptions;

use Exception;

class OrderExists extends Exception
{
    public $frisboOrderId;

    public function __construct($message, $code = 0, Exception $previous = null)
    {
        $this->frisboOrderId = $this->getOrderIdFromMessage($message);
        parent::__construct($message, $code, $previous);
    }
        
    private function getOrderIdFromMessage(string $message): string
    {
        $messageFormat = "Order with reference/id %s already exists.";
        list($frisboOrderReferenceWithOrderId) = sscanf($message, $messageFormat);
        $splitReference = explode("/", $frisboOrderReferenceWithOrderId);
        $orderId = end($splitReference);
        return (string) $orderId;
    }
}
