<?php

namespace Frisbo\FrisboSdk\Caches;

use Frisbo\FrisboSdk\FrisboCache;

class FrisboFileCache extends FrisboCache
{

    public function load(string $key)
    {
        $filename = $this->getFileName($key);
        if (!file_exists($filename)) return false;
        $h = fopen($filename, 'rb');

        if (!$h) return false;

        // Getting a shared lock
        flock($h, LOCK_SH);
        $data = file_get_contents($filename);
        fclose($h);

        $data = json_decode($data);

        if (!$data) {
            // If unserializing somehow didn't work out, we'll delete the file
            unlink($filename);
            return false;
        }

        if (is_readable($filename) && time() > $data[0]) {
            // Unlinking when the file was expired
            unlink($filename);
            return false;
        }

        return $data[1];
    }

    public function save(string $key, string $value, int $expiry = 3600): bool
    {
        // Opening the file in read/write mode
        $h = fopen($this->getFileName($key), 'ab+');
        if (!$h) throw new \RuntimeException('Could not write to cache');

        flock($h, LOCK_EX); // exclusive lock, will get released when the file is closed

        fseek($h, 0); // go to the start of the file

        // truncate the file
        ftruncate($h, 0);

        // Serializing along with the TTL
        $data = json_encode(array(time() + $expiry, $value));
        if (fwrite($h, $data) === false) {
            throw new \RuntimeException('Could not write to cache');
        }
        fclose($h);

        return true;
    }

    public function forget(string $key): bool
    {
        $filename = $this->getFileName($key);
        if (file_exists($filename)) {
            return unlink($filename);
        }

        return false;
    }

    private function getFileName($key)
    {
        return $this->getCacheFolder() . md5($key) . '.cache';
    }

    private function getCacheFolder()
    {
        return $this->options['CACHE_FOLDER'] ?? '/tmp/';
    }
}